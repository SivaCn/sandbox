

##
## given two lists say,
## 
## persons = ['siva', 'albert', 'peter', 'alvin']
## greet = ['Hi', 'Welcome']
##
## for each person in persons list print 
## "Hi <person name> Welcome"
##
## using List compherehension
##

def fun1(persons, greet):
    """."""
    return [ ' {0} '.join(greet).format(each_person) \
             for each_person in persons
           ]

persons = ['siva', 'albert', 'peter', 'alvin']
greet = ['Hi', 'Welcome']

print fun1(persons, greet)
